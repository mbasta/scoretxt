#include <iostream>
using std::cout;
using std::endl;
#include <memory>

#include "logger.h"
#include "miner.h"
#include "page.h"

using namespace score_txt;


int main()
{
	/// @todo add argument to prevent from mining again,
	/// but to use cached results.

	std::unique_ptr<Logger> logger = std::unique_ptr<Logger>(new Logger("last.log"));
	logger->debug("---starting score txt---");
	logger->debug("creating page...");
	Page page(logger.get());
	logger->debug("loading data...");
	//page.load_data_from_file(std::string("../data/score_00.csv"));

	/// @todo create a miner, pass the mined table to the page.
	const std::string la_liga_url = "https://www.laliga.com/en-AT/laliga-easports/standing";
	const std::string la_liga_results_filename = "./bin/laliga.csv";
	LaligaMiner miner_laliga;
	if (!miner_laliga.mine(la_liga_results_filename)) {
		logger->debug("Failed to mine.");
		return 1;
	}

	//page.load_data_from_file(std::string("./data/score_01.csv"));
	page.load_data_from_file(la_liga_results_filename);
	page.load_theme_from_file("./data/examples/theme_00.csv");

	logger->debug("one-time rendering...");
	page.render();


	return 0;
}
