#ifndef SCORETXT_LOGGER_H
#define SCORETXT_LOGGER_H

#include <iostream>
#include <fstream>
#include <string>

namespace score_txt
{

class Logger
{
public:
	Logger(std::string filename);
	void debug(std::string s);
private:
	std::ofstream m_ofs;
    std::string m_filename;
};

}

#endif 
