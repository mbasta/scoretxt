#ifndef SCORETXT_DOWNLOAD_H
#define SCORETXT_DOWNLOAD_H

#include <iostream>
#include <string>
#include <fstream>

#include <stdio.h>

#include <curl/curl.h>

namespace score_txt
{
 
bool download(const std::string& url, const std::string& out_filename)
{
	CURL *curl;
	CURLcode res;
	FILE* out_file = fopen(out_filename.c_str(), "w");
	if (out_file == NULL) {
		return false;
	}

	curl = curl_easy_init();
	if(!curl) {
		return false;
	}

	curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, out_file);

	res = curl_easy_perform(curl);

	if(res != CURLE_OK) {
		std::cerr << "curl_easy_perform() failed: " << curl_easy_strerror(res) << std::endl;
	}

	curl_easy_cleanup(curl);

	if (out_file) {
		fclose(out_file);
	}
	return true;
}

} // namespace

#endif // DOWNLOAD_H
