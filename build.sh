function fail {
    echo $1
    exit 1
}

g++ -std=c++11 -Wall -Wextra -pedantic-errors -c *.cpp || fail "Failed to compile."
mv *.o bin/ || fail "Failed to move the *.o files."
g++ -o bin/score_txt bin/*.o -lcurl || fail "Failed to link the files."
