#ifndef SCORETXT_MINER_H
#define SCORETXT_MINER_H

#include <map>
#include <string>

namespace score_txt
{

enum class League
{
	LA_LIGA,
	PREMIER_LEAGUE,
	BUNDESLIGA,
	SERIE_A
};

struct SourceData
{
	std::string url;
	League league;
};

struct MinedData
{
	int position;
	std::string club_abr;
	std::string club_name;
	int points;
	int played;
	int wins;
	int draws;
	int losses;
	int scored;
	int conceded;
	std::string goal_diff;
};

/// @brief A miner searches the web for data.
/// Each URL source has its own way to mine data.
class Miner
{
public:
	Miner(League league, const std::string& url);
	virtual bool mine(const std::string& out_filename) = 0;
	virtual size_t num_teams() const = 0;
protected:
	League m_league;
	const std::string m_url;
};

class LaligaMiner : public Miner
{
public:
	LaligaMiner();
	/// @brief Mines the data from the URL source.
	/// @param out_filename 
	/// @return 
	bool mine(const std::string& out_filename) override;
	size_t num_teams() const override;
};

} // namespace

#endif // SCORETXT_MINER_H