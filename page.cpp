#include "page.h"

#include <fstream>
#include <map>
#include <sstream>
#include <string>
#include <vector>

#ifdef __linux__
#include <sys/ioctl.h>
#include <unistd.h>
#elif _WIN32
#include <windows.h>
#endif

#include "colors.h"
#include "logger.h"

using std::ifstream;
using std::stringstream;
using std::string;
using std::vector;


namespace score_txt
{

Page::Page(Logger* p_logger)
	: m_logger{ p_logger }
{
	m_output_channels.push_back(&std::cout);
}

std::pair<size_t, size_t> Page::terminal_dim()
{
#ifdef __linux__
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	return std::make_pair<size_t, size_t>(w.ws_row, w.ws_col);
#elif _WIN32
#endif
	return std::make_pair<size_t, size_t>(0, 0);
}

std::string strip(std::string s)
{
    size_t begin = 0;
    size_t end = s.size();
    begin = s.find_first_not_of(" ");
    end = s.find_last_not_of(" ") + 1;
    if (begin == std::string::npos || end == std::string::npos) {
        return s;
    }
    return s.substr(begin, end - begin);
}

void Page::load_data_from_file(std::string filename)
{
	m_logger->debug("Page::load_data_from_file: ENTER");

	vector<string> constraint_types;
	m_columns.clear();
	string row;
	ifstream f_in(filename);
	string cell;

	// 1. row are width constraint types
	std::getline(f_in, row);
	stringstream rowStream(row);
	while(std::getline(rowStream, cell, ',')) {
		constraint_types.push_back(cell);
	}

	m_logger->debug("Page::load_data_from_file: num cols in the 1. row:");
	m_logger->debug(std::to_string(constraint_types.size()));

	// 2. row are width constraint values
	std::getline(f_in, row);
	rowStream = stringstream(row);
	size_t col_idx = 0;
	while(std::getline(rowStream, cell, ',')) {
		WidthConstraint constraint{
			width_constraint_type(strip(constraint_types.at(col_idx))),
			atoi(cell.c_str())
			};
		m_columns.push_back({constraint, vector<string>{}});
		++col_idx;
	}

	m_logger->debug("Page::load_data_from_file: num cols in the 2. row:");
	m_logger->debug(std::to_string(col_idx));

	// The rest are the data to represent.
	m_logger->debug("Page::load_data_from_file: reading data...");
	while(std::getline(f_in, row)) {
		rowStream = stringstream(row);
		col_idx = 0;
		while(std::getline(rowStream, cell, ',')) {
			//m_logger->debug(cell);
			m_columns.at(col_idx).rows.push_back(cell);
			++col_idx;
		}
	}

	m_logger->debug("Page::load_data_from_file: rows loaded for each column:");
	for (const auto& col : m_columns) {
		m_logger->debug(std::to_string(col.rows.size()));
	}
}

void Page::load_theme_from_file(std::string filename)
{
	m_logger->debug("Page::load_theme_from_file: ENTER");

    m_theme = std::map<size_t, std::vector<int>>{};

	string rowLine;
	ifstream f_in(filename);
	string cell;

    // row_no, bkg, frg, bold, underline
	std::getline(f_in, rowLine);

    stringstream rowStream("");
    int row = 0;
    while(std::getline(f_in, rowLine)) {
        size_t col_idx = 0;
        rowStream = stringstream(rowLine);
        while(std::getline(rowStream, cell, ',')) {
            //jhere
            cell = strip(cell);
            m_logger->debug("cell: " + cell);
            switch (col_idx) {
                // row_no
                case 0: {
                    row = atoi(cell.c_str());
                    if (m_theme.find(row) == m_theme.end()) {
                        m_theme[row] = std::vector<int>{};
                    }
                    break;
                }
                // bkg
                case 1: {
                    m_theme[row].push_back(colors[ColorMode::BKG][str_color_code_map[cell]]);
                    break;
                }
                // frg
                case 2: {
                    m_theme[row].push_back(colors[ColorMode::FRG][str_color_code_map[cell]]);
                    break;
                }
                // bold
                case 3: {
                    if (cell == "true") {
                        m_theme[row].push_back(colors[ColorMode::MOD_ON][ColorCode::BOLD]);
                    }
                    break;
                }
                case 4: {
                    if (cell == "true") {
                        m_theme[row].push_back(colors[ColorMode::MOD_ON][ColorCode::UNDERLINE]);
                    }
                    break;
                }
            }
            ++col_idx;
        }
        m_logger->debug("row style:");
        for (auto code : m_theme[row]) { m_logger->debug(std::to_string(code)); }
        ++row;
    }
}


void Page::render()
{
	size_t max_ch = 0;
	size_t col_width_chars = 0;
	auto dim = terminal_dim();
	m_logger->debug("terminal's num rows:");
	m_logger->debug(std::to_string(dim.first));
	m_logger->debug("terminal's num cols:");
	m_logger->debug(std::to_string(dim.second));
    // -1 because terminal prompts for input of the next command in a new line.
	size_t num_rows = dim.first - 1;
	for (std::ostream* ch : m_output_channels) {
		for (size_t row = 0; row < num_rows; ++row) {
			for (const auto& column : m_columns) {
				if (row < column.rows.size()) {
                    if (m_theme.find(row) != m_theme.end()) {
                        text_style(m_theme[row], *ch);
                    } else {
                        text_style({}, *ch);
                    }

					switch (column.width_constraint.constraint_type) {
						typedef WidthConstraint::ConstraintType Type;

						case Type::FIXED_CHAR_WIDTH: {
							col_width_chars = size_t(column.width_constraint.value);
							break;
						}
						case Type::PERC_TO_SCREEN: {
							col_width_chars = size_t(column.width_constraint.value * dim.second / 100);
							break;
						}
						default: {
							// not implemented
                            m_logger->debug("Unsupported width constraint type.");
							break;
						}
					}

					max_ch = std::min(col_width_chars, column.rows.at(row).size());
					(*ch) << column.rows.at(row).substr(0, max_ch);
					for (size_t rest_idx = max_ch; rest_idx < col_width_chars; ++rest_idx) {
						(*ch) << ' ';
					}
				}
			}
			(*ch) << '\n';
		}
	}
}

} //namespace
