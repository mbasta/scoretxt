#ifndef SCORETXT_PAGE_H
#define SCORETXT_PAGE_H

#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "logger.h"

namespace score_txt
{

struct WidthConstraint
{
	enum class ConstraintType
	{
		FIXED_CHAR_WIDTH,
		PERC_TO_LEFT,
		PERC_TO_RIGHT,
		PERC_TO_SCREEN,
		FIT_TEXT,
		FLEXIBLE
	} constraint_type;
	int value;
};

static WidthConstraint::ConstraintType width_constraint_type(std::string s)
{
	typedef WidthConstraint::ConstraintType Type;
	if (s == "FIXED_CHAR_WIDTH") {
		return Type::FIXED_CHAR_WIDTH;
	}
	if (s == "PERC_TO_LEFT") {
		return Type::PERC_TO_LEFT;
	}
	if (s == "PERC_TO_RIGHT") {
		return Type::PERC_TO_RIGHT;
	}
	if (s == "PERC_TO_SCREEN") {
		return Type::PERC_TO_SCREEN;
	}
	if (s == "FIT_TEXT") {
		return Type::FIT_TEXT;
	}
	if (s == "FLEXIBLE") {
		return Type::FLEXIBLE;
	}
	return Type::FIXED_CHAR_WIDTH;
}

struct Column
{
	WidthConstraint width_constraint;
	std::vector<std::string> rows;
};

struct Page
{
public:
	Page(Logger* p_logger);
	void load_data_from_file(std::string filename);
	void load_theme_from_file(std::string filename);

	/* Print out the page to the registered output channels. */
	void render();
private:
	static std::pair<size_t, size_t> terminal_dim();
private:
	// To be replaced by Layout objects.
	Logger* m_logger;
	std::vector<Column> m_columns;
	std::vector<std::ostream*> m_output_channels;
    std::map<size_t, std::vector<int>> m_theme;
};

} // namespace

#endif
