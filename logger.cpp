#include "logger.h"

namespace score_txt
{

Logger::Logger(std::string filename)
    : m_filename{ filename }
{
	m_ofs.open(m_filename);
}

void Logger::debug(std::string s)
{
    m_ofs.open(m_filename, std::ios_base::app);
	m_ofs << s << std::endl;
    m_ofs.close();
}

}
