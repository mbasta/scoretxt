#ifndef SCORETXT_LAYOUT_H
#define SCORETXT_LAYOUT_H

#include <map>
#include <vector>

namespace score_txt
{

struct Field
{
	size_t width_chars;
	size_t height_lines;
};

struct Layout
{
	
};

} // namespace

#endif
