#ifndef SCORETXT_COLORS_H
#define SCORETXT_COLORS_H

#include <iostream>
#include <map>
#include <string>
#include <vector>

namespace score_txt
{

enum class ColorMode { FRG, BKG, MOD_ON, MOD_OFF };
enum class ColorCode { BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE, RESET, BOLD, UNDERLINE, INVERSE};

std::ostream& text_style(std::vector<int> color_codes, std::ostream& o = std::cout)
{
	o << "\033[";
	for (auto code : color_codes) {
		o << ';' << code;
	}
	o << "m";
	return o;
}

std::map<ColorMode, std::map<ColorCode, int>> colors = {
	{ ColorMode::FRG, {
		{ ColorCode::BLACK, 30 },
		{ ColorCode::RED, 31 },
		{ ColorCode::GREEN, 32 },
		{ ColorCode::YELLOW, 33 },
		{ ColorCode::BLUE, 34 },
		{ ColorCode::MAGENTA, 35 },
		{ ColorCode::CYAN, 36 },
		{ ColorCode::WHITE, 37 }
		}
	},
	{ ColorMode::BKG, {
		{ ColorCode::BLACK, 40 },
		{ ColorCode::RED, 41 },
		{ ColorCode::GREEN, 42 },
		{ ColorCode::YELLOW, 44 },
		{ ColorCode::BLUE, 44 },
		{ ColorCode::MAGENTA, 45 },
		{ ColorCode::CYAN, 46 },
		{ ColorCode::WHITE, 47 }
		}
	},
	{ ColorMode::MOD_ON, {
		{ ColorCode::RESET, 0 },
		{ ColorCode::BOLD, 1 },
		{ ColorCode::UNDERLINE, 4 },
		{ ColorCode::INVERSE, 7 }
		}
	},
	{ ColorMode::MOD_OFF, {
		{ ColorCode::BOLD, 21 },
		{ ColorCode::UNDERLINE, 24 },
		{ ColorCode::INVERSE, 27 }
		}
	}
}; // map colors

std::map<std::string, ColorMode> str_color_mode_map = {
    { "bkg", ColorMode::BKG },
    { "frg", ColorMode::FRG },
    { "mod_on", ColorMode::MOD_ON },
    { "mod_off", ColorMode::MOD_OFF }
};

std::map<std::string, ColorCode> str_color_code_map = {
    { "black", ColorCode::BLACK },
    { "red", ColorCode::RED },
    { "green", ColorCode::GREEN },
    { "yellow", ColorCode::YELLOW },
    { "blue", ColorCode::BLUE },
    { "magenta", ColorCode::MAGENTA },
    { "cyan", ColorCode::CYAN },
    { "white", ColorCode::WHITE },
    { "reset", ColorCode::RESET },
    { "bold", ColorCode::BOLD },
    { "underline", ColorCode::UNDERLINE },
    { "inverse", ColorCode::INVERSE }
};
    

} // namespace

#endif
