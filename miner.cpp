#include "miner.h"

#include <fstream>
#include <limits>
#include <vector>

#include "download.h"

namespace score_txt
{

Miner::Miner(League league, const std::string& url)
	: m_league{ league },
	m_url{ url }
{
}

LaligaMiner::LaligaMiner()
	: Miner(League::LA_LIGA, "https://www.laliga.com/en-AT/laliga-easports/standing")
{}

/// @brief Search for the text in the `line` piece of HTML code.
/// Begin at `begin` index of `line`, never look further than `max_count` characters.
/// Store the found text in `out_text`.
/// Return the index of the `line` after which the found text ends.
/// If no text is found for whatever reason, return std::string::npos.
/// An empy string counts as a text found if all other conditions were met.
size_t next_text(const std::string& line, size_t begin, size_t max_count, std::string& out_text)
{
	//jhere
	const std::string ref_str = "styled__TextStyled-sc-";
	size_t idx = line.find(ref_str, begin);
	size_t idx_start = line.find(">", idx + ref_str.size() + 1);
	size_t idx_end = line.find("</p", idx_start + 1);
	if (idx == std::string::npos || idx_start == std::string::npos || idx_end == std::string::npos) {
		std::cerr << "begin: " << begin << std::endl;
		std::cerr << "idx: " << idx << std::endl;
		std::cerr << "idx_start: " << idx << std::endl;
		std::cerr << "idx_end: " << idx << std::endl;
		return std::string::npos;
	}
	out_text = line.substr(idx_start + 1, idx_end - idx_start - 1);
	return idx_end;
}

bool write_mined_data(const std::vector<MinedData>& mined_data, const std::string& out_filename)
{
	std::ofstream ofs(out_filename);
	if (!ofs.is_open()) {
		std::cerr << "Failed to open file " << out_filename << std::endl;
		return false;
	}

	/// @todo choose a theme

	for (size_t i = 0; i < 10; ++i) {
		ofs << "PERC_TO_SCREEN, ";
	}
	ofs << "PERC_TO_SCREEN" << std::endl;
	ofs << "5, 5, 50, 5, 5, 5, 5, 5, 5, 5, 5" << std::endl;
	ofs << "Prog., Pos., Team, Played, Wins, Draws, Losses, Scored, Conceded, GDiff, PTS" << std::endl;

	for (const auto& data : mined_data) {
		ofs << 0 << ", " // To calculate this, we need data from the past fixture.
			<< data.position << "., "
			<< data.club_name << ", "
			<< data.played << ", "
			<< data.wins << ", "
			<< data.draws << ", "
			<< data.losses << ", "
			<< data.scored << ", "
			<< data.conceded << ", "
			<< data.goal_diff << ", "
			<< data.points
			<< std::endl;
	}

	return true;
}

bool LaligaMiner::mine(const std::string& out_filename)
{
	std::string tmp_download_filename = out_filename + ".html";
	if (!download(m_url, tmp_download_filename)) {
		std::cerr << "Failed to download the file." << std::endl;
		return false;
	}

	std::ifstream ifs(tmp_download_filename);
	if (!ifs.is_open()) {
		std::cerr << "Failed to open the downloaded file." << std::endl;
		return false;
	}
	/// @todo extract data from the HTML file.

	// First off: find the line which contains `<div class="show"`.
	// It's a unique string (even `"show"` (including the quotes) is a unique string).
	// This div is the 3rd parent of the part representing the table.
	//
	// There's this `class="styled__StandingTableBody`, which is the direct parent of the divs representing the table rows.
	// But it appears 3 times in the code.
	//
	// `class="styled__ContainerAccordion-*"` represents each table row.
	// But it appears 60 times (and there are 20 table rows).
	//
	// `styled__StandingTabBody-sc-e89col-0 isRHqh` holds the data, but it also appears 60 (3x20) times.
	//
	// It seems like the code has 3 tables, but only one is shown.
	//
	// But it doesn't matter. The following line appears 3 times, but only the 1st time matters:
	//
	// <div class="styled__StandingTabBody-sc-e89col-0 isRHqh"><div class="styled__Td-sc-e89col-10 fTFWtb"><p class="styled__TextStyled-sc-1mby3k1-0 iBRpyN">1</p>
	//
	// It seems each table is defined in one line.
	// We could find the first occurence of any of the above, extract that line and then follow the patters,
	// because the rest of the 3 occurances are on other lines.
	//
	// The columns are:
	// Position, Club Abr, Club name, Pts, Played, W, D, L, G scored, G conceded, G diff

	std::vector<MinedData> mined_data;
	mined_data.reserve(num_teams());

	std::string line;
	bool line_found = false;
	while(std::getline(ifs, line)) {
		if (line.find("<div class=\"styled__StandingTabBody") != std::string::npos) {
			line_found = true;
			break;
		}
	}

	if (!line_found) {
		std::cerr << "Failed to find the relevant line in the HTML code." << std::endl;
		return false;
	}

	const size_t table_num_teams = num_teams();
	size_t cursor = 0;
	std::string last_text;
	for (size_t row_idx = 0; row_idx < table_num_teams; ++row_idx) {
		MinedData new_data;
		int position = row_idx + 1;
		cursor = next_text(line, cursor, std::numeric_limits<size_t>::max(), last_text);
		// We ignore the position deliberately.
		/// @todo read the position non the less,
		/// for the case where two teams share the position number.
		if (cursor == std::string::npos) {
			std::cerr << "Failed to find position for row " << row_idx << std::endl;
			return false;
		}
		new_data.position = position;

		cursor = next_text(line, cursor, 10000, last_text);
		if (cursor == std::string::npos) {
			std::cerr << "Failed to find club abr for row " << row_idx << std::endl;
			return false;
		}
		new_data.club_abr = last_text;

		cursor = next_text(line, cursor, 10000, last_text);
		if (cursor == std::string::npos) {
			std::cerr << "Failed to find club name for row " << row_idx << std::endl;
			return false;
		}
		new_data.club_name = last_text;

		cursor = next_text(line, cursor, 10000, last_text);
		if (cursor == std::string::npos) {
			std::cerr << "Failed to find points for row " << row_idx << std::endl;
			return false;
		}
		new_data.points = atoi(last_text.c_str());

		cursor = next_text(line, cursor, 10000, last_text);
		if (cursor == std::string::npos) {
			std::cerr << "Failed to find played for row " << row_idx << std::endl;
			return false;
		}
		new_data.played = atoi(last_text.c_str());

		cursor = next_text(line, cursor, 10000, last_text);
		if (cursor == std::string::npos) {
			std::cerr << "Failed to find wins for row " << row_idx << std::endl;
			return false;
		}
		new_data.wins = atoi(last_text.c_str());

		cursor = next_text(line, cursor, 10000, last_text);
		if (cursor == std::string::npos) {
			std::cerr << "Failed to find draws for row " << row_idx << std::endl;
			return false;
		}
		new_data.draws = atoi(last_text.c_str());

		cursor = next_text(line, cursor, 10000, last_text);
		if (cursor == std::string::npos) {
			std::cerr << "Failed to find losses for row " << row_idx << std::endl;
			return false;
		}
		new_data.losses = atoi(last_text.c_str());

		cursor = next_text(line, cursor, 10000, last_text);
		if (cursor == std::string::npos) {
			std::cerr << "Failed to find scored for row " << row_idx << std::endl;
			return false;
		}
		new_data.scored = atoi(last_text.c_str());

		cursor = next_text(line, cursor, 10000, last_text);
		if (cursor == std::string::npos) {
			std::cerr << "Failed to find conceded for row " << row_idx << std::endl;
			return false;
		}
		new_data.conceded = atoi(last_text.c_str());

		cursor = next_text(line, cursor, 10000, last_text);
		if (cursor == std::string::npos) {
			std::cerr << "Failed to find goal diff for row " << row_idx << std::endl;
			return false;
		}
		new_data.goal_diff = last_text;

		mined_data.push_back(new_data);
	}

	/// @todo write to the output file
	if (!write_mined_data(mined_data, out_filename)) {
		std::cerr << "Failed to write the mined data (" << mined_data.size() << ") to the file " << out_filename << std::endl;
		return false;
	}

	return true;
}

size_t LaligaMiner::num_teams() const
{
	return 20;
}

} // namespace
